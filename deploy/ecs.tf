resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

# Create "task_execution_role_policy" in AWS IAM Policy
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving of images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json") # this json file will be sent to create in AWS policy console
}

# Create "task_execution_role" in AWS IAM Role
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# Attach AWS IAM Role Policy named "task_execution_role_policy" to the role named "task_execution_role"
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

# Create "app_iam_role" in AWS IAM Role
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# Create "ecs_task_logs" in AWS CloudWatch log group
resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}

# Whenever we add new data here, we have to init terraform again
data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  vars = {
    app_image                = var.ecr_image_api
    proxy_image              = var.ecr_image_proxy
    django_secret_key        = var.django_secret_key
    db_host                  = aws_db_instance.main.address
    db_name                  = aws_db_instance.main.name
    db_user                  = aws_db_instance.main.username
    db_pass                  = aws_db_instance.main.password
    log_group_name           = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region         = data.aws_region.current.name
    allowed_hosts            = aws_route53_record.app.fqdn # set allowed_hosts to lb address
    s3_storage_bucket_name   = aws_s3_bucket.app_public_files.bucket
    s3_storage_bucket_region = data.aws_region.current.name
  }
}

resource "aws_ecs_task_definition" "api" {
  family                   = "${local.prefix}-api"                                 # name of the task definition
  container_definitions    = data.template_file.api_container_definitions.rendered # rendered means pass in all the values and replace in template file
  requires_compatibilities = ["FARGATE"]                                           # FARGATE is a type of ecs hosting which allows us to host container without managing server
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.task_execution_role.arn # Give the permissions to execute a new task
  task_role_arn            = aws_iam_role.app_iam_role.arn        # Role given to actual running task (permissions at runtime)

  volume {
    name = "static"
  }

  tags = local.common_tags
}

# Create aws_security_group named "ecs_service"
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  egress { # this will allow outbound access from our container on port 443
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { # this will allow outbound access from port 5432 to the cidr_blocks database is running in
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  ingress { # Accept inbound connections from public internet to proxy on port 8000, not app port 9000. The reason is all connections must go via proxy
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }

  tags = local.common_tags
}

resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.api.family
  desired_count   = 1         # this is number of tasks we wish to run inside the service
  launch_type     = "FARGATE" # serverless docker deployment process

  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }

  load_balancer { # Tell ecs service to register new tasks with our target group. This is for allow lb forward requests to running registered tasks
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy" # tell the target group to forward the request to container named proxy within our application which is running on port 8000 
    container_port   = 8000
  }

  depends_on = [aws_lb_listener.api_https] # tells terraform to create lb listener first -> Set dependencies between resources to ensure that terraform create resources in the right order
}

data "template_file" "ecs_s3_write_policy" { # this prepare the template file with substituting variables
  template = file("./templates/ecs/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn
  }
}

# create a new policy
resource "aws_iam_policy" "ecs_s3_access" {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow access to the recipe app S3 bucket"

  policy = data.template_file.ecs_s3_write_policy.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}