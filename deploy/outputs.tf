# Output of a certain variable or attribution on one of our resources
# When we deploy terraform, we can see configurations of our server

output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" { # output lb dns_name once it's been created by Terraform
  value = aws_route53_record.app.fqdn
}

