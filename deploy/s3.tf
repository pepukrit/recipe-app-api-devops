resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-pep-michael904"
  acl           = "public-read" # Access control list. Allow bucket to be publicly read
  force_destroy = true          # Allow terraform easily destroy bucket
}