resource "aws_lb" "api" { # create aws_lb
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets = [ # load_balancer has to be on the public subnet. Because, we want to access LB via internet
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}

# Target group: Group of servers that the load balancer can forward requests to
resource "aws_lb_target_group" "api" { # create aws_lb_target_group. A group of ECS tasks
  name        = "${local.prefix}-api"
  protocol    = "HTTP"          # load balancer will send request to our api by using http protocol
  vpc_id      = aws_vpc.main.id # load balancer target group in the same VPC as our application
  target_type = "ip"            # adding the addresses to the lb using ip address
  port        = 8000            # port that gonna forward request on. Port proxy  will run on in ECS task

  health_check { # feature to perform regular polls on our application to ensure it's running
    path = "/admin/login/"
  }
}

#  Its job is to accept the request and send them to target group which distributes the request to our group of services that are running.
resource "aws_lb_listener" "api" { # create lb listener (Entry proint in to the load balancer)
  load_balancer_arn = aws_lb.api.arn
  port              = 80 # forward requests coming from port 80 to the target group. This means forwarding requests to running tasks (application)
  protocol          = "HTTP"

  default_action { # is how we want to handle requests to our listener. In this case, it forwards the request to target_group
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301" # http redirect response
    }
  }
}

resource "aws_lb_listener" "api_https" { # create new lb listener (Entry proint in to the load balancer) using https
  load_balancer_arn = aws_lb.api.arn     # specify the same load balancer
  port              = 443                # forward requests coming from port 80 to the target group. This means forwarding requests to running tasks (application)
  protocol          = "HTTPS"

  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn # specify the certificate handling requests to lb
  # this will tell terraform to find certificate validation cert before creating the listener

  default_action { # is how we want to handle requests to our listener. In this case, it forwards the request to target_group
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}


resource "aws_security_group" "lb" { # create security group of lb, which allows access into our load balancer
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  ingress { # Accept all connections from the public internet in to our load balancer
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { # Accept all connections from the public internet in to our load balancer
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { # Give lb access on port 8000 on all cidr_blocks, which forwards request on port 8000 to our application
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}