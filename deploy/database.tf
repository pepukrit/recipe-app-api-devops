resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [ # this allows adding subnets to the database
    aws_subnet.private_a.id,
    aws_subnet.private_b.id,
  ]

  tags = merge( # some kind of glitch, workaround is to merge it in tag
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id # define VPC for our security group

  ingress { # allow the database to connect with inbound access via port 5432 (default)
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    security_groups = [ # this mean access will be limited to the resources that have security group attached below
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id, # Give access to our ECS service from our database
    ]
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {              # this will create RDS instance
  identifier              = "${local.prefix}-db" # database identifier used to access the database in our console
  name                    = "recipe"             # used to define the database name that is created within our instance
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "11.4"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  multi_az                = false # define if the database should run in multi availability zone
  skip_final_snapshot     = true  # mechanism to protect data loss when destroy/remove database, this will generate final snapshot of database
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}