data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}." # standard name convention ended with . for route 53 zone
}

resource "aws_route53_record" "app" { # records can be added to zone in order to create subdomain
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}" # look up items and return one that matched with current env
  type    = "CNAME"                                                                            # Canonical name links it to another existing DNS name that wants to forward request to
  ttl     = "300"                                                                              # Time to live. Everytime we make changes to DNS, it will take up to 5 mins to affect

  records = [aws_lb.api.dns_name]
}

resource "aws_acm_certificate" "cert" {           # create a new cert for our domain name
  domain_name       = aws_route53_record.app.fqdn # fqdn = fully qualified domain name
  validation_method = "DNS"                       # validate domain name by using DNS

  tags = local.common_tags

  lifecycle {
    create_before_destroy = true # Keep Terraform running smooth
  }
}

resource "aws_route53_record" "cert_validation" { # create a record for the purpose of domain validation
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value # some kind of random string created when we create certificate, used to validate that we own the domain name
  ]
  ttl = "60" # Keep this as the lowest point to make sure that we can run a terraform within a minute
}

resource "aws_acm_certificate_validation" "cert" {                    # not a real resource in Terraform. Used to trigger the validation process
  certificate_arn         = aws_acm_certificate.cert.arn              # cert that we want to validate
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn] # pass validation record
}