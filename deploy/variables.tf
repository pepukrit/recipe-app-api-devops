variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "pepukrit3@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion" # name matched with key pair in aws ec2
}

variable "ecr_image_api" { # setting the default variable that allows us to deploy from local machine
  description = "ECR image for API"
  default     = "945018260791.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" { # setting the default variable that allows us to deploy from local machine
  description = "ECR image for proxy"
  default     = "945018260791.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "bangkokappdev.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string) # map key value below to string
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}